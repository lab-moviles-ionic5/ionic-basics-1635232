const tipoInput = document.querySelector('#input-tipo')
const montoInput = document.querySelector('#input-monto')
const guardarBtn = document.querySelector('#btn-guardar')

const gastosList = document.querySelector('#list-gastos')
const totalOutput = document.querySelector('#output')

let total = 0;

function clear(){
    tipoInput.value = '';
    montoInput.value = '';
}

guardarBtn.addEventListener('click', ()=>{
    const tipo = tipoInput.value;
    const monto = montoInput.value;

    if(tipo.trim().length > 0 &&
      monto.trim().length > 0 &&
      monto > 0)  
    {
        //agregamos el item a la lista

        const newitem = document.createElement('ion-item');
        newitem.textContent = tipo + ' : $' + monto;
        gastosList.appendChild(newitem);

        // totalizamos

        total += +monto;
        totalOutput.textContent = total;

        clear(); //limpamos inputs
    }
    else{
        console.error('Valores invalidos !');
        alert('Valores invalidos! ');
        alertController.create({
            message: 'Llene los campos correctamente',
            header: 'Valores invalidos',
            buttons: ['Ok']
        }).then(alertElement => {
            alertElement.present();
        });
    }
});